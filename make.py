import sys
import shutil
from pathlib import Path
from typing import Iterator
import subprocess

ROOT = Path(sys.path[0])
SRC_DIR = ROOT / "src"
PREPROCESSED_DIR = ROOT / "preprocessed/"
BUILT_DIR = ROOT / "built/"
MOCHA_HOOKS_FILE = BUILT_DIR / "tests/hooks.js"
# TESTS_TSCONFIG = ROOT / "tsconfig-test.json"


def ts_files_in_dir(dir_: Path) -> Iterator[Path]:
    listing: Path
    for listing in dir_.iterdir():
        if not listing.is_file(): continue

        file_ext = listing.name.rpartition('.')[2]
        if file_ext.lower() != 'ts': continue
        yield listing

def error_checked_run(*args, **kwargs):
    result = subprocess.run(*args, **kwargs)
    if result.returncode != 0:
        raise EnvironmentError("Got return code:", result.returncode)

def test() -> None:
    assert MOCHA_HOOKS_FILE.is_file()
    error_checked_run(['npx', 'mocha',
                       '--require', MOCHA_HOOKS_FILE,
                       "'built/tests/**/*.js'"],
                      shell=True)

def compile_() -> None:
    shutil.rmtree(BUILT_DIR)
    BUILT_DIR.mkdir()
    error_checked_run(['npx', 'tsc', '--build', '--verbose'], shell=True)


if __name__ == "__main__":
    compile_()

    test()
