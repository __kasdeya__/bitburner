import * as kaslib from "./lib/kaslib.js";

export async function main(ns: any) {
    for (let host of kaslib.getAllHosts(ns)) {
        ns.tprint(
            "lvl=", ns.getServerRequiredHackingLevel(host).toString().padEnd(3),
            "\tports=", ns.getServerNumPortsRequired(host),
            "\tmax=$", ns.formatNumber(ns.getServerMaxMoney(host)),
            "\tram=", ns.formatRam(ns.getServerMaxRam(host)),
            "\tminSec=", ns.getServerMinSecurityLevel(host),
            "\t", ns.hasRootAccess(host) ? "*" : " ",
            host,
        );
    }
}
