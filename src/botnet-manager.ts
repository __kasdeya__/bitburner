import * as kaslib from "./lib/kaslib.js";
import * as botnet from "./lib/botnet.js";

const SCRIPT_NAME = "botnet-manager.js";
const WEAKEN_SCRIPT = "weaken.js";
const HACK_SCRIPT = "hack.js";
const GROW_SCRIPT = "grow.js";

export async function main(ns: any) {
    if (ns.args.length != 1) {
        usage(ns);
        return;
    }

    const target = ns.args[0];

    while (true) {
        // `HACK_SCRIPT` uses the least RAM of all the zombie scripts
        while (botnet.getTotalBotnetThreads(ns, HACK_SCRIPT) <= 0) {
            await ns.sleep(500);
        }

        const weakensNeeded = kaslib.getWeakensNeeded(ns, target);

        if (weakensNeeded > 0) {
            botnet.dispatchTask(ns, WEAKEN_SCRIPT, weakensNeeded, target);
        } else if (ns.getServerMoneyAvailable(target) == ns.getServerMaxMoney(target)) {
            const totalThreads = botnet.getTotalBotnetThreads(ns, HACK_SCRIPT);
            botnet.dispatchTask(ns, HACK_SCRIPT, totalThreads, target);
        } else {
            const totalThreads = botnet.getTotalBotnetThreads(ns, GROW_SCRIPT);
            botnet.dispatchTask(ns, GROW_SCRIPT, totalThreads, target);
        }
    }
}

function usage(ns: any) {
    ns.tprint("Usage: ", SCRIPT_NAME, " <target host>");
}
