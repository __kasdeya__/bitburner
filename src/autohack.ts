import * as kaslib from "./lib/kaslib.js";

const SCRIPT_NAME = "autohack.js";
const WEAKEN_SCRIPT = "weaken.js";
const HACK_SCRIPT = "hack.js";
const GROW_SCRIPT = "grow.js";

export async function main(ns: any) {
    //ns.tail();

    ns.disableLog('disableLog');

    // otherwise sleep() will spam the log while polling
    ns.disableLog('sleep');

    // we print these ourselves
    ns.disableLog('getServerMaxMoney');
    ns.disableLog('getServerMoneyAvailable');
    ns.disableLog('getServerSecurityLevel');
    ns.disableLog('getServerMinSecurityLevel');
    ns.disableLog('run');

    // I'm confident that all code involving these calls is good, so no need to print them
    ns.disableLog('getServerMaxRam');
    ns.disableLog('getServerUsedRam');

    if (ns.args.length != 1) {
        usage(ns);
        return;
    }

    const target = ns.args[0];
    while (true) {
        // TODO: Some targets require a *lot* of growing and we end up raising security endlessly without weakening again (which makes the growing take much longer)
        await fullyWeaken(ns, target);
        await fullyGrow(ns, target);
        await fullyWeaken(ns, target);
        await fullyHack(ns, target);
    }
}

function usage(ns: any) {
    ns.tprint("Usage: " + SCRIPT_NAME + " <target>");
}

async function fullyGrow(ns: any, target: string) {
    while (ns.getServerMoneyAvailable(target) < ns.getServerMaxMoney(target)) {
        const maxThreads = kaslib.getMaxThreads(ns, GROW_SCRIPT, ns.getHostname());
                ns.print("fullyGrow ", target, ": ",
            ns.formatNumber(ns.getServerMoneyAvailable(target)),
            "/",
            ns.formatNumber(ns.getServerMaxMoney(target)),
            " growing with ", maxThreads, " threads",
        );
        await kaslib.asyncRun(ns, GROW_SCRIPT, maxThreads, target);
    }
    ns.print("fullyGrow ", target, ": done growing.");
}

async function fullyHack(ns: any, target: string) {
    while (ns.getServerMoneyAvailable(target) >= ns.getServerMaxMoney(target) * 0.75) {
        const maxThreads = kaslib.getMaxThreads(ns, GROW_SCRIPT, ns.getHostname());
        const moneyPreHack = ns.getServerMoneyAvailable(target);
        await kaslib.asyncRun(ns, HACK_SCRIPT, maxThreads, target);
        const moneyPostHack = ns.getServerMoneyAvailable(target);
        ns.print(
            "fullyHack ", target, ": $",
            ns.formatNumber(moneyPreHack),
            " -> $",
            ns.formatNumber(moneyPostHack),
            " / $",
            ns.formatNumber(ns.getServerMaxMoney(target)),
            " (got $", ns.formatNumber(moneyPreHack - moneyPostHack), ") ",
            "hacked with ", maxThreads, " threads",
        );
    }
}

async function fullyWeaken(ns: any, target: string) {
    const maxThreads = kaslib.getMaxThreads(ns, WEAKEN_SCRIPT, ns.getHostname());
    let weakensNeeded = kaslib.getWeakensNeeded(ns, target);

    while (weakensNeeded > 0) {
        const threads = Math.min(weakensNeeded, maxThreads);
        ns.print("Need ", weakensNeeded, " weakens. Weakening with ", threads, "/", maxThreads, " threads.")
        await kaslib.asyncRun(ns, WEAKEN_SCRIPT, threads, target);
        weakensNeeded = kaslib.getWeakensNeeded(ns, target);
    }
}
