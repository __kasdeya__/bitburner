const SCRIPT_NAME = "weaken.js";

export async function main(ns: any) {
    if (ns.args.length < 1 || ns.args.length > 2) {
        usage(ns);
        return;
    }

    const host = ns.args[0];
    const threads = Number(ns.args[1]);

    await ns.weaken(host, {'threads': threads});
}

function usage(ns: any) {
    ns.tprint("Usage: run " + SCRIPT_NAME + " <host> [-t <threads>]");
}
