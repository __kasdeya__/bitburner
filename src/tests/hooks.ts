import * as sinon from 'sinon';

export let mochaHooks = {
    afterEach() {
        // reset sinon after each test.
        // https://sinonjs.org/releases/latest/general-setup/
        sinon.restore();
    },
};
