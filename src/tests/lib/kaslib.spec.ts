import * as assert from 'assert';
import * as sinon from 'sinon';

import * as kaslib from '../../lib/kaslib.js';

declare var describe: (description: string, func: Function) => void;
declare var it: (description: string, func: Function) => void;

describe('getMaxThreads()', () => {
    it('should calculate threads correctly', () => {
        let ns: any = {
            "getScriptRam": sinon.fake.returns(10),
            "getServerMaxRam": sinon.fake.returns(40),
            "getServerUsedRam": sinon.fake.returns(1),
        };

        assert.equal(kaslib.getMaxThreads(ns, "scriptname", "hostname"), 3);
        assert.ok(ns.getScriptRam.alwaysCalledWith("scriptname", "hostname"));
        assert.ok(ns.getServerMaxRam.calledWithExactly("hostname"));
        assert.ok(ns.getServerUsedRam.calledWithExactly("hostname"));
    });
});

describe('asyncRun()', () => {
    it('should wait until isRunning returns false', async () => {
        const RUN_PID = 1337;
        let ns: any = {
            "isRunning": sinon.stub(),
            "sleep": sinon.fake.resolves(null),
            "run": sinon.fake.returns(RUN_PID),
        };

        ns.isRunning.onCall(3).returns(false);
        ns.isRunning.returns(true);

        await kaslib.asyncRun(ns, "foo", "bar", "baz");
        assert.ok(ns.run.calledOnce);
        assert.ok(ns.run.firstCall.calledWithExactly("foo", "bar", "baz"));
        assert.ok(ns.isRunning.alwaysCalledWith(RUN_PID));
        assert.equal(ns.isRunning.callCount, 4);
        assert.equal(ns.sleep.callCount, 3);
    });
});

describe('getAllHosts()', () => {
    it('should scan through all hosts on the network', () => {
        let ns: any = {
            "scan": sinon.stub(),
        };

        ns.scan.throws("invalid hostname");
        ns.scan.withArgs("home").returns(["a", "b"]);
        ns.scan.withArgs("a").returns(["b", "home"]);
        ns.scan.withArgs("b").returns(["c", "d"]);
        ns.scan.withArgs("c").returns(["a", "b", "home"])
        ns.scan.withArgs("d").returns([]);

        const result = kaslib.getAllHosts(ns);

        assert.equal(result.length, 5);
        assert.notEqual(result.indexOf("home"), -1);
        assert.notEqual(result.indexOf("a"), -1);
        assert.notEqual(result.indexOf("b"), -1);
        assert.notEqual(result.indexOf("c"), -1);
        assert.notEqual(result.indexOf("d"), -1);

        assert.equal(ns.scan.callCount, 5);
        assert.ok(ns.scan.calledWithExactly("home"));
        assert.ok(ns.scan.calledWithExactly("a"));
        assert.ok(ns.scan.calledWithExactly("b"));
        assert.ok(ns.scan.calledWithExactly("c"));
        assert.ok(ns.scan.calledWithExactly("d"));
    });
});

describe('getWeakensNeeded()', () => {
    it('should perform the calculation correctly', () => {
        let ns: any = {
            "getServerSecurityLevel": sinon.fake.returns(2.01),
            "getServerMinSecurityLevel": sinon.fake.returns(1),
        };

        const TARGET = "hostname";
        assert.equal(kaslib.getWeakensNeeded(ns, TARGET), 21);

        assert.ok(ns.getServerSecurityLevel.calledOnceWithExactly(TARGET));
        assert.ok(ns.getServerMinSecurityLevel.calledOnceWithExactly(TARGET));
    });
});
