import * as assert from 'assert';
import * as sinon from 'sinon';

import * as botnet from '../../lib/botnet.js';

declare var describe: (description: string, func: Function) => void;
declare var it: (description: string, func: Function) => void;

/*
describe("getAllZombies()", () => {
    it("should find and filter hosts correctly", () => {
        // TODO: How the fuck do I stub `botnet.isZombie`?
        // error TS2540: Cannot assign to 'isZombie' because it is a read-only property.
        // botnet.isZombie = sinon.stub();

        // TypeError: Cannot assign to read only property 'isZombie' of object '[object Module]'
        // (botnet.isZombie as any) = sinon.stub();

        // TypeError: ES Modules cannot be stubbed
        sinon.stub(botnet, "isZombie");
    });
});
//*/

describe("isZombie()", () => {
    it("should match its answer to that of ns.hasRootAccess()", () => {
        let ns: any = {
            "hasRootAccess": sinon.stub(),
        };

        ns.hasRootAccess.withArgs("foo").returns(false);
        ns.hasRootAccess.withArgs("bar").returns(true);

        assert.equal(botnet.isZombie(ns, "foo"), false);
        assert.ok(ns.hasRootAccess.calledOnceWithExactly("foo"));

        assert.equal(botnet.isZombie(ns, "bar"), true);
        assert.equal(ns.hasRootAccess.callCount, 2);
        assert.ok(ns.hasRootAccess.secondCall.calledWithExactly("bar"));
    });
});
