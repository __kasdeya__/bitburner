import * as kaslib from "./kaslib.js";
import { filename, hostname } from "./kaslib.js";
// const ZOMBIE_FILES = ["grow.js", "hack.js", "weaken.js"];

/**
 * Returns the number of threads (0 if none) that we weren't able to dispatch
 * for this task. (because there wasn't enough RAM left on the botnet)
 */
export function dispatchTask(
    ns: any, script: filename, threads: number, target: hostname
): number {
    for (const [host, threadsAvail] of getBotnetThreads(ns, script)) {
        if (threads <= 0) { break; }
        if (threadsAvail <= 0) { continue; }

        const threadsToUse = Math.min(threadsAvail, threads);
        ns.exec(script, host, threadsToUse, target);
        threads -= threadsToUse;
        ns.print('Assigned ', threads, 'x ', script, ' to ', host);
    }

    return threads;
}

/**
 * Get all of the threads available in the botnet (like with `getMaxThreads`),
 * organized by which host has which threads available.
 */
export function getBotnetThreads(
    ns: any, script: filename
): [host: hostname, threadsAvail: number][] {
    return getAllZombies(ns).map(
        (host) => [host, kaslib.getMaxThreads(ns, script, host)]);
}

export function getTotalBotnetThreads(ns: any, script: filename): number {
    return getBotnetThreads(ns, script)
        .reduce((sum, [_, threadsAvail]) => sum + threadsAvail, 0);
}

export function getAllZombies(ns: any): hostname[] {
    return kaslib.getAllHosts(ns).filter((host) => isZombie(ns, host));
}

export function isZombie(ns: any, host: hostname): boolean {
    if (ns.hasRootAccess(host) == false) {
        return false;
    }

    // for (const zombieFile of ZOMBIE_FILES) {
    //     if (ns.fileExists(zombieFile, host) == false) {
    //         return false;
    //     }
    // }

    return true;
}
