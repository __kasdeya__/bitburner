export type hostname = string;
export type filename = string;

// how much does one weaken() decrease security level by?
const WEAKEN_DECREASE = 0.05;

/**
 * Call with args to ns.run
 */
export async function asyncRun(ns: any, ...args: any[]) {
    const pid = ns.run(...args);
    while (ns.isRunning(pid)) {
        await ns.sleep(500);
    }
}

/**
 * Get the max number of threads that `script` can be run with on `host`
 */
export function getMaxThreads(ns: any, script: filename, host: hostname) {
    const scriptRam = ns.getScriptRam(script, host);
    const freeRam = ns.getServerMaxRam(host) - ns.getServerUsedRam(host);
    return Math.floor(freeRam / scriptRam);
}

/**
 * Literally every host that we can see from `home`
 */
export function getAllHosts(ns: any): hostname[] {
    let hosts: hostname[] = ["home"];
    for (let i = 0; i < hosts.length; i++) {
        for (const host of ns.scan(hosts[i])) {
            if (hosts.indexOf(host) == -1) {
                hosts.push(host);
            }
        }
    }

    return hosts;
}

/**
 * How many times do we need to weaken this host to get it down to its minimum
 * security level?
 */
export function getWeakensNeeded(ns: any, target: hostname) {
    const softSecurity = ns.getServerSecurityLevel(target) - ns.getServerMinSecurityLevel(target);
    return Math.ceil(softSecurity / WEAKEN_DECREASE);
}
